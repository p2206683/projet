#                                                      TP noté Git - Durée : 3 heures

Instructions :
* Nous allons utiliser le dépôt disponible au lien suivant :

[https://forge.univ-lyon1.fr/sebastien.gadrat/tuto-git-fusion.git](https://forge.univ-lyon1.fr/sebastien.gadrat/tuto-git-fusion.git)   

* Faites une copie de ce dépôt dans votre espace personnel (un "fork"), puis cloner ce dépôt localement.

* Suivez les instructions, et réalisez les actions dans l'ordre indiqué.

* Vous avez le droit d'utiliser les documents de cours ainsi que les pages de manuel de Git.

* Toutes les commandes Git utilisées seront données dans le compte-rendu (ou insérées dans le README.md), et effectuées par la suite.

* La note finale tiendra compte de vos réponses, mais aussi de la qualité de vos messages de commit.


## I. Prise en main du dépôt (30 minutes – 4 points)

On travaillera dans un dépôt local.

1. Sur quelle branche êtes-vous ?

We are on the main branch.

```git branch```

2. Combien de branches contient ce dépôt ?

This repository contains 3 branches.

```git ls-remote --heads https://forge.univ-lyon1.fr/p2206683/projet | wc -l```

3. Pourquoi certaines branches apparaissent-elles en rouge ?

Some branches appear in red because they are not merged with the parent branch.

4. À quoi sert le fichier `.gitignore` ? Que contient-il ?

The **.gitignore** file is used to ignore files in the current directory. It contains extensions of files or files.

5. Créez une nouvelle branche `test` à partir de la branche `main`.

```git checkout -b test main```

6. Ajoutez une fonction `foobar()` au code C qui renvoie un entier. Elle ressemblera à ça :
```
int foobar() {
	return 1;
}
```		
Et on ajoutera cela dans la fonction main() :
```    
int test = foobar();
printf("foobar returns :%d\n", test);
```
La compilation du code se fera comme suit, en utilisant le fichier `Makefile` :

    $ make

Ou directement en utilisant la commande :

    $ gcc -o example example.c

7. Faites un commit avec vos modifications.

```git commit -a -m "premier commit"```

8. Poussez votre branche sur le dépôt distant.

```git push https://forge.univ-lyon1.fr/p2206683/projet test```

## II. Fusion de branches avec conflit (30 minutes – 4 points)

1. Revenez sur la branche `main`.

```git checkout main```

2. Les deux stratégies principales de fusion de branches sont appelées par avance rapide, ou encore "fast-forward" (anglais), et récursive ou "non-fast-forward" (anglais). Quelle est la principale différence entre ces deux stratégies de fusion (qui représentent les deux principalement utilisées).

The main difference between the two branch merge strategies in Git is that fast-forward merge does not require the creation of a new merge commit, while recursive or "non-fast-forward" merge requires the creation of a new merge commit.

3. Laquelle est la plus communément utilisée dans la gestion d’un projet collaboratif ? Pourquoi ?

The "fast-forward" merge strategy is the most commonly used in collaborative project management with Git, because it allows to maintain a linear and clear development line.

4. Fusionnez la branche `develop` dans la branche `main` en utilisant la stratégie de fusion dite "non-fast-forward".

```git merge --no-ff develop```

5. Il y a conflit… Quelle commande puis-je faire pour annuler la fusion en cours ?

```git merge –abort```

6. Corrigez le conflit de fusion en modifiant le code de manière appropriée. On suppose ici que c’est le code de la branche develop qui est celui à conserver.

we copy the content of the example.c file from the develop branch to the main branch

```git commit –a –m « deuxième commit »```

```git merge --no-ff develop```

7. Faites un commit avec vos modifications.

```git commit –a –m « deuxième commit »```

8. Poussez votre branche sur le dépôt distant.

```git push https://forge.univ-lyon1.fr/p2206683/projet main```

## III. Fusion de branches avec stratégie octopus (1 heure, 6 points)

Maintenant que vous avez compris comment fusionner des branches avec les stratégies de fusion standard, nous allons voir comment utiliser la stratégie octopus pour fusionner plusieurs branches en une seule opération.

1. Récupérez les dernières modifications

Comme précédemment, vous devez vous assurer que vous êtes à jour avec les dernières modifications des branches `develop` et `feature`. Pour cela, effectuez les commandes suivantes :
```
$ git switch develop
$ git pull
$ git switch feature
$ git pull
```
* Que fait la commande `git pull` ? Décrire son fonctionnement.

The "git pull" command is used to retrieve the latest changes from a remote repository (such as GitHub) and merge them with the local changes. In other words, it synchronizes your local branch with the remote branch.

First, Git fetches changes from the remote repository using the "git fetch" command. This downloads all the changes from the remote repository without merging them with the local changes.

Then Git merges the changes retrieved from the remote repository with the local changes using the "git merge" command. If conflicts arise between the local and remote changes, Git will ask you to resolve the conflicts before continuing the merge.
If the merge goes through without conflicts, Git applies the merged changes to your local branch.

* Pourquoi exécuter ces commandes ?

These commands allow to check that the develop and feature repository are up to date with the remote repository and update it in the opposite case

2. Fusionner les branches avec octopus

Maintenant que vous êtes à jour, vous pouvez fusionner les branches avec la stratégie octopus. Cette stratégie permet de fusionner plusieurs branches en une seule opération.
```
$ git switch main
$ git merge develop feature
```
3. Résoudre les conflits

Si des conflits surgissent lors de la fusion, vous devrez les résoudre en utilisant les mêmes techniques que celles décrites précédemment.
La version à conserver est celle contenant les fonctions `bar()` et `baz()`.

```git commit -a -m "quatrième commit"```

4. Tester la compilation

Une fois que la fusion est terminée, vous devez tester que le code compile correctement.
Si le code compile correctement, vous avez terminé la partie III de l'examen. Cependant, dans ce cas précis, vous allez constater que le code ne compile pas. En effet, la fusion avec la branche feature a introduit un bug. Il va donc falloir utiliser `git bisect` pour trouver le commit responsable de l'introduction de ce bug.

## III. Utilisation de Git bisect pour déterminer le commit introduisant le bug (1 heure, 6 points)
   
1. Comment peut-on afficher l’historique de la branche courante ? Qu'est-ce que l'identifiant d'un commit ? Où le trouve-t-on ?

To display the history of the current branch, you can use the **git log** command.

The commit ID is a unique string generated by Git to uniquely identify each commit in a Git repository.
It can be found in the output of the **git log** command

2. Comment peut-on visualiser les différences entre deux branches ? Entre deux commits ? Entre deux branches entre le dépôt local et le dépôt distant ?

To visualize the difference between two branches and two commits, you can use the **git diff** command. 

For example, ```git diff <branch1> <branch2>``` and ```git diff <commit1> <commit2>```

3. On va utiliser `git bisect` pour déterminer le commit introduisant le bug.

4. Exécutez la commande `git bisect start`.

5. Exécutez la commande `git bisect bad HEAD` pour marquer le commit actuel comme étant mauvais.

6. Trouvez un commit connu pour être bon, par exemple le premier commit dans la branche main, et de l'exécuter en utilisant la commande `git bisect good <commit-id>`.

7. Vérifiez si le commit actuel est bon ou mauvais en compilant le code.

8. Exécutez `git bisect good` ou `git bisect bad` en fonction du résultat de l'étape précédente pour marquer le commit courant comme étant bon ou mauvais.

9. Répétez les étapes 6 et 7 jusqu'à ce que le commit introduisant le bug soit identifié (processus itératif).

10. Terminez le processus de bisect en utilisant la commande `git bisect reset`.

11. Placez-vous sur le commit qui a introduit le bug en créant une nouvelle branche, appelée `bug1`, corrigez le bug sur cette nouvelle branche en suivant la proposition donnée lors de la tentative de compilation, puis effectuer une fusion sur main pour corriger ce bug.

12. De nouveau sur la branche main, compiler le code. Que se passe-t-il ?

We can see that we always have error messages.

13. Répétez les étapes précédentes pour corriger ce second bug (en créant une nouvelle branche `bug2`), et obtenir un code qui compile.

14. Annulez ce dernier commit correcteur.

15. Nous allons voir ici une autre manière de porter le commit qui corrige le bug sur la branche main : on utilisera ici la commande `git cherry-pick` (qui agira comme un patch). Pour cela, placez-vous sur main, et, avec cette commande, importer et jouer le commit correcteur sur la branche `main`.
